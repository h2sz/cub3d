/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_functions1.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 16:50:46 by ksam              #+#    #+#             */
/*   Updated: 2020/10/29 01:44:41 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	gest_error(t_data *img, int code_error)
{
	if (code_error != 0)
	{
		mlx_destroy_window(img->mlx, img->mlx_win);
		mlx_destroy_image(img->mlx, img->img);
		ft_memdel((void *)&(img->sp_order));
		ft_memdel((void *)&(img->sp_dist));
		ft_memdel((void *)&(img->path_to_east_texture));
		ft_memdel((void *)&(img->path_to_north_texture));
		ft_memdel((void *)&(img->path_to_south_texture));
		ft_memdel((void *)&(img->path_to_west_texture));
		ft_memdel((void *)&(img->path_to_sprite_texture));
		ft_memdel((void *)&(img->zbuffer));
	}
	gest_error_next(img, code_error);
	exit(1);
}

void	gest_error_next(t_data *img, int code_error)
{
	int i;

	i = 0;
	if (code_error > 0 && code_error < 7)
		ft_printf("Error :\nInvalid texture.");
	if (code_error > 1)
		mlx_destroy_image(img->mlx, img->north_ptr);
	if (code_error > 2)
		mlx_destroy_image(img->mlx, img->south_ptr);
	if (code_error > 3)
		mlx_destroy_image(img->mlx, img->west_ptr);
	if (code_error > 4)
		mlx_destroy_image(img->mlx, img->east_ptr);
	if (code_error > 5)
		mlx_destroy_image(img->mlx, img->sprite_ptr);
	if (img->worldmap != NULL)
	{
		while (img->countline >= 0)
		{
			free(img->worldmap[img->countline]);
			img->worldmap[img->countline] = NULL;
			img->countline--;
		}
	}
	free(img->worldmap);
}

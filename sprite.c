/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/05 03:44:50 by ksam              #+#    #+#             */
/*   Updated: 2020/10/28 17:37:45 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	calculate_sprite(t_data *img, int x)
{
	int			k;
	t_sprites	*sprites;

	k = 0;
	sprites = malloc(sizeof(t_sprites) * (img->sprite_counter + 1));
	calculate_sprite_dist(img, sprites);
	x = (int)x;
	img->sp_x = sprites[img->sp_order[x]].x - img->posx;
	img->sp_y = sprites[img->sp_order[x]].y - img->posy;
	img->inv_det = 1.0 / (img->planex * img->diry - img->dirx *
		img->planey);
	img->transx = img->inv_det * (img->diry * img->sp_x - img->dirx
		* img->sp_y);
	img->transy = img->inv_det * (-img->planey * img->sp_x +
		img->planex * img->sp_y);
	img->sp_screen = (int)((img->swidth / 2) * (1 + img->transx /
		img->transy));
	img->sp_h = abs((int)(img->sheight / img->transy));
	img->drawstarty = -img->sp_h / 2 + img->sheight / 2;
	calculate_sprite_next(img);
	free(sprites);
	sprites = NULL;
}

void	calculate_sprite_next(t_data *img)
{
	if (img->drawstarty < 0)
		img->drawstarty = 0;
	img->drawendy = img->sp_h / 2 + img->sheight / 2;
	if (img->drawstarty >= img->sheight)
		img->drawstarty = img->sheight - 1;
	img->sp_w = abs((int)(img->sheight / (img->transy)));
	img->drawstartx = -img->sp_w / 2 + img->sp_screen;
	if (img->drawstartx < 0)
		img->drawstartx = 0;
	img->drawendx = img->sp_w / 2 + img->sp_screen;
	if (img->drawendx >= img->swidth)
		img->drawendx = img->swidth - 1;
}

void	calculate_sprite_dist(t_data *img, t_sprites *sprites)
{
	int			i;
	int			j;
	int			k;

	i = -1;
	k = 0;
	while (img->worldmap[++i] != '\0')
	{
		j = -1;
		while (img->worldmap[i][++j] != '\0')
			if (img->worldmap[i][j] == '2')
			{
				sprites[k].y = i + 0.5;
				sprites[k++].x = j + 0.5;
			}
	}
	i = -1;
	while (++i < img->sprite_counter)
	{
		img->sp_order[i] = i;
		img->sp_dist[i] = ((img->posx - sprites[i].x) * (img->posx -
			sprites[i].x) + (img->posy - sprites[i].y) * (img->posy -
			sprites[i].y));
	}
	calculate_sort_dist(img);
}

void	calculate_sort_dist(t_data *img)
{
	int			i;
	int			j;
	int			tmp;
	double		tmp1;

	j = 0;
	i = -1;
	while (++i < img->sprite_counter)
	{
		j = i - 1;
		while (++j < img->sprite_counter)
		{
			if (img->sp_dist[i] < img->sp_dist[j])
			{
				tmp = img->sp_order[i];
				img->sp_order[i] = img->sp_order[j];
				img->sp_order[j] = tmp;
				tmp1 = img->sp_dist[i];
				img->sp_dist[i] = img->sp_dist[j];
				img->sp_dist[j] = tmp1;
				i = -1;
				j = img->sprite_counter;
			}
		}
	}
}

void	draw_sprite(t_data *img)
{
	int		i;

	while (img->drawstartx < img->drawendx && img->drawstartx < img->swidth)
	{
		i = img->drawstarty;
		img->tex_x = (int)(256 * (img->drawstartx - (-img->sp_w / 2
			+ img->sp_screen)) * img->sprite_width / img->sp_w) / 256;
		if (img->drawstartx < img->swidth && img->transy > 0 &&
		img->drawstartx > 0 && img->transy < img->zbuffer[img->drawstartx])
		{
			while (++i < img->drawendy && i < img->sheight)
			{
				img->calc = i * 256 - img->sheight * 128 + img->sp_h * 128;
				draw_sprite_next(img);
				if ((img->color & 0xffffff) != 0 && img->swidth *
					img->sheight > i * img->swidth + img->drawstartx)
					img->addr[i * img->swidth +
						img->drawstartx] = img->color;
			}
		}
		img->drawstartx++;
	}
}

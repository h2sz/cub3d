/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_function2.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/27 05:40:47 by ksam              #+#    #+#             */
/*   Updated: 2020/10/28 20:18:24 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	camera_init(t_data *bag)
{
	int	i;
	int	ret;

	i = 0;
	ret = -1;
	if (bag->worldmap != NULL)
	{
		i = find_cardinal(bag);
		if (bag->cardinal != NULL)
		{
			bag->posy = (double)i;
			ret = ft_strichr(bag->worldmap[i], bag->cardinal[0]);
			if (ret != -1)
				bag->posx = (double)ret;
			bag->worldmap[i][ret] = '0';
		}
	}
}

int		assign_cardinal(t_data *bag, int i)
{
	if ((bag->cardinal = ft_strchr(bag->worldmap[i], 'N')))
	{
		vector_north(bag);
		return (1);
	}
	if ((bag->cardinal = ft_strchr(bag->worldmap[i], 'S')))
	{
		vector_south(bag);
		return (1);
	}
	if ((bag->cardinal = ft_strchr(bag->worldmap[i], 'E')))
	{
		vector_east(bag);
		return (1);
	}
	if ((bag->cardinal = ft_strchr(bag->worldmap[i], 'W')))
	{
		vector_west(bag);
		return (1);
	}
	return (0);
}

int		find_cardinal(t_data *bag)
{
	int i;

	i = 0;
	while (bag->worldmap[i])
	{
		if (assign_cardinal(bag, i))
			break ;
		i++;
	}
	return (i);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/27 01:06:43 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:25:38 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

int			check_left(t_data *img, int x, int y)
{
	if (img->copy[y][x] == '1')
		return (1);
	else if (img->copy[y][x] == '5' || x == 0)
		return (0);
	return (check_left(img, x - 1, y));
}

int			check_right(t_data *img, int x, int y)
{
	if (img->copy[y][x] == '1')
		return (1);
	else if (img->copy[y][x] == '5' || x == img->len_max)
		return (0);
	return (check_right(img, x + 1, y));
}

int			check_up(t_data *img, int x, int y)
{
	if (img->copy[y][x] == '1')
		return (1);
	else if (img->copy[y][x] == '5' || y == 0)
		return (0);
	return (check_up(img, x, y - 1));
}

int			check_down(t_data *img, int x, int y)
{
	if (img->copy[y][x] == '1')
		return (1);
	else if (img->copy[y][x] == '5' || y == img->countline)
		return (0);
	return (check_down(img, x, y + 1));
}

int			is_a_zero(t_data *img, int x, int y)
{
	if (!(check_left(img, x, y)))
		return (0);
	if (!(check_right(img, x, y)))
		return (0);
	if (!(check_up(img, x, y)))
		return (0);
	if (!(check_down(img, x, y)))
		return (0);
	return (1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_functions3.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/09 05:59:36 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:29:34 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	color_wall(t_data *img)
{
	if (img->side == 0 && img->raydirx > 0)
		img->tex_y = (int)img->texpos & img->east_height - 1;
	else if (img->side == 0 && img->raydirx < 0)
		img->tex_y = (int)img->texpos & img->west_height - 1;
	else if (img->side == 1 && img->raydiry < 0)
		img->tex_y = (int)img->texpos & img->north_height - 1;
	else
		img->tex_y = (int)img->texpos & img->south_height - 1;
	if (img->side == 0 && img->raydirx > 0)
		img->color = img->data_east[img->east_height * img->tex_y
													+ img->tex_x];
	else if (img->side == 0 && img->raydirx < 0)
		img->color = img->data_west[img->west_height * img->tex_y
													+ img->tex_x];
	else if (img->side == 1 && img->raydiry < 0)
		img->color = img->data_north[img->north_height
										* img->tex_y + img->tex_x];
	else
		img->color = img->data_south[img->south_height * img->tex_y
													+ img->tex_x];
	img->texpos += img->step;
}

void	verline(t_data *img)
{
	int y;

	y = img->drawstart;
	while (y <= img->drawend)
	{
		color_wall(img);
		img->addr[y++ * img->swidth + img->x] = img->color;
	}
}

void	draw_floor_ceiling(t_data *img)
{
	int		y;

	y = img->drawend;
	while (y < img->sheight)
	{
		img->addr[y * img->swidth + img->x] = img->floor_color;
		img->addr[(img->sheight - y - 1)
					* img->swidth + img->x] = img->ceiling_color;
		y++;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 21:56:18 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:29:34 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	texture(t_data *img)
{
	if (img->side == 0 && img->raydirx > 0)
		texture_east(img);
	if (img->side == 0 && img->raydirx < 0)
		texture_west(img);
	if (img->side == 1 && img->raydiry < 0)
		texture_north(img);
	if (img->side == 1 && img->raydiry > 0)
		texture_south(img);
}

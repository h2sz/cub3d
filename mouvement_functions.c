/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouvement_functions.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 12:36:20 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:30:10 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	move(t_data *img)
{
	img->movespeed = img->frametime * 4.5;
	img->rotspeed = img->frametime * 4.5;
	if (img->move_up == 1)
		move_up(img);
	if (img->move_down == 1)
		move_down(img);
	if (img->move_directional_left == 1)
		move_directional_left(img);
	if (img->move_directional_right == 1)
		move_directional_right(img);
	if (img->rotation_left == 1)
		rotation_left(img);
	if (img->rotation_right == 1)
		rotation_right(img);
}

void	move_directional_right(t_data *img)
{
	if (img->worldmap[(int)(img->posy)][(int)(img->posx
				- img->planex * img->movespeed)] == '0')
		img->posx += img->planex * img->movespeed;
	if (img->worldmap[(int)(img->posy - img->planey
			* img->movespeed)][(int)(img->posx)] == '0')
		img->posy += img->planey * img->movespeed;
}

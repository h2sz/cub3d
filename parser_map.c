/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 01:13:06 by ksam              #+#    #+#             */
/*   Updated: 2020/10/28 20:10:26 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"
#include <stdio.h>

int		parsing_map(char *line, t_data *bag)
{
	size_t i;

	bag->countline += 1;
	bag->worldmap = ft_realloc(bag->worldmap, sizeof(char*)
						* (bag->countline + 1), sizeof(char*) * bag->countline);
	i = count_valid_char(line, bag);
	if (i != ft_strlen(line))
	{
		ft_putstr_fd("Error :\nWrong char on map", 2);
		exit(1);
	}
	else
		bag->worldmap[bag->countline] = ft_strdup(line);
	return (0);
}

int		count_valid_char(char *line, t_data *img)
{
	int i;

	i = 0;
	while (ft_isdigit(line[i]) || line[i] == ' ' || line[i] == 'N' ||
	line[i] == 'S' || line[i] == 'E' || line[i] == 'W' || line[i] == '\t')
	{
		if (line[i] == 'N' || line[i] == 'S' || line[i] == 'E' ||
			line[i] == 'W')
			img->cardinal_count++;
		if (line[i] == '2')
			img->sprite_counter++;
		i++;
	}
	return (i);
}

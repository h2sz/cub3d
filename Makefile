# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/04 12:37:16 by ksam              #+#    #+#              #
#    Updated: 2020/10/29 01:54:59 by ksam             ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

# **************************************************************************** #
#                                    VARIABLES                                 #
# **************************************************************************** #

default: all

OS = $(shell uname)

NAME    =	cub3D

HEADER	=	includes/cub3d.h

PRINTF	=	libftprintf.a

PRINTFS	=	ft_printf/libftprintf.a

LIBFTS	=	ft_printf/libft/libft.a

SRCS	=	cub3d.c \
			cub3d_utils.c \
			check_map.c \
			check_map2.c \
			check_map3.c \
			display_functions.c \
			display_functions1.c \
			display_functions2.c \
			display_functions3.c \
			display.c \
			error_functions.c \
			error_functions1.c \
			events_keycode.c \
			get_next_line.c \
			get_next_line_utils.c \
			initialize_function.c \
			initialize_function2.c \
			initialize_function3.c \
			initialize_texture.c \
			mouvement_functions.c \
			mouvement_functions1.c \
			parser_floor_ceiling.c \
			parser_map.c \
			parser_resolution.c \
			parser_texture.c \
			parser.c \
			texture.c \
			texture_utils.c \
			screenshot.c \
			sprite.c \
			sprite1.c

OBJS	=	$(SRCS:.c=.o)

CC 		= clang

RM		=	rm -f

# CFLAGS	=	-Wall -Wextra -Werror -g3 -fsanitize=address -Iincludes/ -Ift_printf/include/ -Ift_print/libft/
CFLAGS	=	-Wall -Wextra -Werror -g3 -Iincludes/ -Ift_printf/include/ -Ift_print/libft/

MLX		=	-lmlx -lm -framework OpenGL -framework AppKit

MLX_L	=	-lX11 -lXext -lmlx -lm -lbsd

# **************************************************************************** #
#                                    RULES                                     #
# **************************************************************************** #

all		:	$(PRINTF) $(NAME)

# %.o		:	%.c $(HEADER)
# ifeq (${OS}, Darwin)
# 	 		$(CC) $(CFLAGS) -c -I./minilibx/ $< -o $@
# else
# 	 		$(CC) $(CFLAGS) -c $< -o $@
# endif

$(NAME)	:	$(OBJS) $(HEADER)
			$(MAKE) -C minilibx/ all
ifeq (${OS}, Darwin)
			# ${CC} -o $@ ${OBJS} -L./minilibx/ $(MLX)
			$(CC) $(CFLAGS) -o $@ $(SRCS) $(PRINTFS) -Lminilibx -Lft_printf $(MLX)
else
			# ${CC} -o $@ ${OBJS} -L./minilibx/ $(MLX_L)
			$(CC) $(CFLAGS) -o $@ $(SRCS) $(PRINTFS) -Lminilibx -Lft_printf $(MLX_L)
endif

$(PRINTF)	:
			$(MAKE) -C ft_printf all

clean	:
			$(MAKE) -C ft_printf $@
			$(MAKE) -C minilibx $@
			$(RM) $(OBJS)

fclean	: 	clean
			$(RM) $(LIBFTS)
			$(RM) $(PRINTFS)
			$(RM) $(NAME)

re		:	fclean all

.PHONY	:	clean fclean all re

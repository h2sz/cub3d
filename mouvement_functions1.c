/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouvement_functions1.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 14:16:56 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:30:10 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	move_down(t_data *img)
{
	if (img->worldmap[(int)(img->posy - img->diry
								* img->movespeed)][(int)(img->posx)] == '0')
		img->posy -= img->diry * img->movespeed;
	if (img->worldmap[(int)(img->posy)][(int)(img->posx - img->dirx
													* img->movespeed)] == '0')
		img->posx -= img->dirx * img->movespeed;
}

void	move_up(t_data *img)
{
	if (img->worldmap[(int)(img->posy)][(int)(img->posx + img->dirx
													* img->movespeed)] == '0')
		img->posx += img->dirx * img->movespeed;
	if (img->worldmap[(int)(img->posy + img->diry
									* img->movespeed)][(int)(img->posx)] == '0')
		img->posy += img->diry * img->movespeed;
}

void	rotation_left(t_data *img)
{
	double olddirx;
	double oldplanex;

	oldplanex = img->planex;
	olddirx = img->dirx;
	img->dirx = img->dirx * cos(-img->rotspeed) - img->diry
										* sin(-img->rotspeed);
	img->diry = olddirx * sin(-img->rotspeed) + img->diry
										* cos(-img->rotspeed);
	img->planex = img->planex * cos(-img->rotspeed)
							- img->planey * sin(-img->rotspeed);
	img->planey = oldplanex * sin(-img->rotspeed)
							+ img->planey * cos(-img->rotspeed);
}

void	rotation_right(t_data *img)
{
	double olddirx;
	double oldplanex;

	oldplanex = img->planex;
	olddirx = img->dirx;
	img->dirx = img->dirx * cos(img->rotspeed) - img->diry
														* sin(img->rotspeed);
	img->diry = olddirx * sin(img->rotspeed) + img->diry
														* cos(img->rotspeed);
	img->planex = img->planex * cos(img->rotspeed) - img->planey
														* sin(img->rotspeed);
	img->planey = oldplanex * sin(img->rotspeed) + img->planey
														* cos(img->rotspeed);
}

void	move_directional_left(t_data *img)
{
	if (img->worldmap[(int)(img->posy)][(int)(img->posx - img->planex
													* img->movespeed)] == '0')
		img->posx -= img->planex * img->movespeed;
	if (img->worldmap[(int)(img->posy - img->planey
									* img->movespeed)][(int)(img->posx)] == '0')
		img->posy -= img->planey * img->movespeed;
}

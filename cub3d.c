/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 14:22:41 by ksam              #+#    #+#             */
/*   Updated: 2020/10/28 17:39:18 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "includes/cub3d.h"

int	main(int ac, char **av)
{
	t_data	img;

	ft_init(&img);
	if (check_arg(ac, av, &img) != 0)
		return (0);
	parser(&img, av[1]);
	window_init(&img);
	init_texture(&img);
	if (img.screenshot == 1)
	{
		display(&img);
		display_sprite(&img);
		save_bitmap("CUB3D.bmp", &img);
		ft_printf("\n>>> Screenshot done ! <<<");
		close_window(&img);
		return (0);
	}
	calculate(&img);
	hook(img);
	return (0);
}

int	hook(t_data img)
{
	mlx_hook(img.mlx_win, 2, 1, event_key_down, &img);
	mlx_hook(img.mlx_win, 3, 2, event_key_up, &img);
	mlx_hook(img.mlx_win, 17, 1L << 17, close_window, &img);
	mlx_loop_hook(img.mlx, calculate, &img);
	mlx_loop(img.mlx);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 21:56:58 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:29:34 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	texture_east(t_data *img)
{
	img->wallx = img->posy + img->perpwalldist * img->raydiry;
	img->wallx -= floor((img->wallx));
	img->tex_x = (int)(img->wallx * (double)img->east_width);
	img->tex_x = img->east_width - img->tex_x - 1;
	img->step = 1.0 * img->east_height / img->lineheight;
	img->texpos = (img->drawstart - img->sheight / 2 + img->lineheight / 2)
	* img->step;
}

void	texture_west(t_data *img)
{
	img->wallx = img->posy + img->perpwalldist * img->raydiry;
	img->wallx -= floor((img->wallx));
	img->tex_x = (int)(img->wallx * (double)img->west_width);
	img->tex_x = img->west_width - img->tex_x - 1;
	img->step = 1.0 * img->west_height / img->lineheight;
	img->texpos = (img->drawstart - img->sheight / 2 + img->lineheight / 2)
	* img->step;
}

void	texture_north(t_data *img)
{
	img->wallx = img->posx + img->perpwalldist * img->raydirx;
	img->wallx -= floor((img->wallx));
	img->tex_x = (int)(img->wallx * (double)img->north_width);
	img->tex_x = img->north_width - img->tex_x - 1;
	img->step = 1.0 * img->north_height / img->lineheight;
	img->texpos = (img->drawstart - img->sheight / 2 + img->lineheight / 2)
	* img->step;
}

void	texture_south(t_data *img)
{
	img->wallx = img->posx + img->perpwalldist * img->raydirx;
	img->wallx -= floor((img->wallx));
	img->tex_x = (int)(img->wallx * (double)img->south_width);
	img->tex_x = img->south_width - img->tex_x - 1;
	img->step = 1.0 * img->south_height / img->lineheight;
	img->texpos = (img->drawstart - img->sheight / 2 + img->lineheight / 2)
	* img->step;
}

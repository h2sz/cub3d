/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events_keycode.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 14:19:04 by ksam              #+#    #+#             */
/*   Updated: 2020/10/25 05:05:57 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

int		event_key_down(int keycode, t_data *img)
{
	int onoff;

	onoff = 1;
	mouvement_bool(keycode, img, onoff);
	return (0);
}

int		event_key_up(int keycode, t_data *img)
{
	int onoff;

	onoff = 0;
	if (keycode == 53 || keycode == KEY_ESC)
		close_window(img);
	mouvement_bool(keycode, img, onoff);
	return (0);
}

void	mouvement_bool(int keycode, t_data *img, int onoff)
{
	img->bool_key = onoff;
	if (keycode == 126 || keycode == KEY_W || keycode == KEY_UP)
		img->move_up = onoff;
	else if (keycode == 125 || keycode == KEY_S || keycode == KEY_DOWN)
		img->move_down = onoff;
	if (keycode == 124 || keycode == KEY_LEFT)
		img->rotation_left = onoff;
	else if (keycode == 123 || keycode == KEY_RIGHT)
		img->rotation_right = onoff;
	if (keycode == 0 || keycode == KEY_A)
		img->move_directional_left = onoff;
	else if (keycode == 2 || keycode == KEY_D)
		img->move_directional_right = onoff;
}

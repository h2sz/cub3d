/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_function.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 14:20:50 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:30:10 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

static void		ft_init_two(t_data *img)
{
	img->frametime = 0;
	img->time = 0;
	img->oldtime = 0;
	img->planex = 0.66;
	img->planey = 0;
	img->movespeed = 0.1;
	img->rotspeed = 0.1;
	img->screenshot = 0;
	img->len_max = 0;
	img->copy = NULL;
}

void			ft_init(t_data *img)
{
	img->mlx = 0;
	img->x = 0;
	img->mapx = 0;
	img->mapy = 0;
	img->sidedistx = 0;
	img->sidedisty = 0;
	img->deltadistx = 0;
	img->deltadisty = 0;
	img->camerax = 0;
	img->rayposx = 0;
	img->rayposy = 0;
	img->raydirx = 0;
	img->raydiry = 0;
	img->perpwalldist = 0;
	img->stepx = 0;
	img->stepy = 0;
	img->hit = 0;
	img->side = 0;
	img->lineheight = 0;
	img->drawstart = 0;
	img->drawend = 0;
	img->color = 0;
	ft_init_two(img);
}

void			parse_init(t_data *img)
{
	img->swidth = -42;
	img->sheight = -42;
	img->path_to_north_texture = NULL;
	img->path_to_south_texture = NULL;
	img->path_to_west_texture = NULL;
	img->path_to_east_texture = NULL;
	img->path_to_sprite_texture = NULL;
	img->floor_color = -1;
	img->ceiling_color = -1;
	img->worldmap = NULL;
	img->countline = -1;
	img->cardinal = NULL;
	img->sprite_counter = 0;
	img->cardinal_count = 0;
	img->bool_key = 0;
	img->move_up = 0;
	img->move_down = 0;
	img->rotation_left = 0;
	img->rotation_right = 0;
	img->move_directional_left = 0;
	img->move_directional_right = 0;
}

void			window_init(t_data *img)
{
	img->mlx = mlx_init();
	if (img->screenshot == 0)
		img->mlx_win = mlx_new_window(img->mlx, img->swidth, img->sheight,
																	"Cub3D");
	img->img = mlx_new_image(img->mlx, img->swidth, img->sheight);
	img->addr = (int *)mlx_get_data_addr(img->img, &img->bits_per_pixel,
								&img->line_length, &img->endian);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 14:19:50 by ksam              #+#    #+#             */
/*   Updated: 2020/10/28 23:12:24 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

int				close_window(t_data *data)
{
	int i;

	i = 0;
	if (data->screenshot == 0)
		mlx_destroy_window(data->mlx, data->mlx_win);
	while (data->countline >= 0)
	{
		free(data->worldmap[data->countline]);
		data->worldmap[data->countline] = NULL;
		free(data->copy[data->countline]);
		data->copy[data->countline] = NULL;
		data->countline--;
	}
	free(data->worldmap);
	free(data->copy);
	free(data->zbuffer);
	free(data->sp_order);
	free(data->sp_dist);
	exit(1);
}

void			ft_memdel(void **str)
{
	free(*str);
	*str = NULL;
}

static	void	destroy_create_image(t_data *img)
{
	mlx_destroy_image(img->mlx, img->img);
	img->img = mlx_new_image(img->mlx, img->swidth, img->sheight);
	img->addr = (int *)mlx_get_data_addr(img->img, &img->bits_per_pixel,
								&img->line_length, &img->endian);
}

int				calculate(t_data *img)
{
	move(img);
	display(img);
	display_sprite(img);
	mlx_put_image_to_window(img->mlx, img->mlx_win, img->img, 0, 0);
	fps_count(img);
	destroy_create_image(img);
	return (0);
}

void			fps_count(t_data *img)
{
	char	*fps;

	fps = NULL;
	img->oldtime = img->time;
	img->time = clock();
	img->frametime = (img->time - img->oldtime) / 1000000;
	if (img->frametime > 0)
		fps = ft_itoa(1.0 / img->frametime);
	mlx_string_put(img->mlx, img->mlx_win, 25, 25, 0x000000, "FPS :");
	mlx_string_put(img->mlx, img->mlx_win, 66, 25, 0x000000, fps);
	free(fps);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_resolution.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 16:48:46 by ksam              #+#    #+#             */
/*   Updated: 2020/10/29 01:19:40 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	is_bigger_width(t_data *bag)
{
	if (bag->swidth > 1920)
		bag->swidth = 1920;
}

void	is_bigger_height(t_data *bag)
{
	if (bag->sheight > 1080)
		bag->sheight = 1080;
}

void	multi_reso(t_data *bag)
{
	if (bag->swidth > -42 || bag->sheight > -42)
	{
		ft_printf("Error :\nDouble 'R' key detected");
		gest_error(bag, 0);
	}
}

t_data	*resolution_fill(t_data *bag, char *line)
{
	int i;

	multi_reso(bag);
	i = 1;
	bag->swidth = ft_atoi(line + i);
	is_bigger_width(bag);
	if (bag->swidth < 1)
	{
		ft_printf("Error :\n R wrong argument");
		gest_error(bag, 0);
	}
	i++;
	while (ft_isdigit(line[i]))
		i++;
	bag->sheight = ft_atoi(line + i);
	is_bigger_height(bag);
	if (bag->sheight < 1)
	{
		ft_printf("Error :\n R wrong argument");
		gest_error(bag, 0);
	}
	return (bag);
}

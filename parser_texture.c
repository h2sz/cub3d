/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_texture.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 15:10:24 by ksam              #+#    #+#             */
/*   Updated: 2020/09/01 09:44:03 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

t_data		*no_texture_fill(t_data *bag, char *line)
{
	int i;

	i = find_dot(line);
	if (!bag->path_to_north_texture)
		bag->path_to_north_texture = ft_strdup(line + i);
	else
	{
		ft_printf("Error :\nDouble 'NO' key detected");
		gest_error(bag, 0);
	}
	return (bag);
}

t_data		*so_texture_fill(t_data *bag, char *line)
{
	int i;

	i = find_dot(line);
	if (!bag->path_to_south_texture)
		bag->path_to_south_texture = ft_strdup(line + i);
	else
	{
		ft_printf("Error :\nDouble 'SO' key detected");
		gest_error(bag, 0);
	}
	return (bag);
}

t_data		*we_texture_fill(t_data *bag, char *line)
{
	int i;

	i = find_dot(line);
	if (!bag->path_to_west_texture)
		bag->path_to_west_texture = ft_strdup(line + i);
	else
	{
		ft_printf("Error :\nDouble 'WE' key detected");
		gest_error(bag, 0);
	}
	return (bag);
}

t_data		*ea_texture_fill(t_data *bag, char *line)
{
	int i;

	i = find_dot(line);
	if (!bag->path_to_east_texture)
		bag->path_to_east_texture = ft_strdup(line + i);
	else
	{
		ft_printf("Error :\nDouble 'EA' key detected");
		gest_error(bag, 0);
	}
	return (bag);
}

t_data		*sprite_texture_fill(t_data *bag, char *line)
{
	int i;

	i = find_dot(line);
	if (!bag->path_to_sprite_texture)
		bag->path_to_sprite_texture = ft_strdup(line + i);
	else
	{
		ft_printf("Error :\nDouble 'S' key detected");
		gest_error(bag, 0);
	}
	return (bag);
}

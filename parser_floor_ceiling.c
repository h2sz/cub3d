/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_floor_ceiling.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/07 02:14:23 by ksam              #+#    #+#             */
/*   Updated: 2020/10/29 01:35:19 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

t_data	*floor_fill(t_data *bag, char *line)
{
	t_color color;
	int		i;

	if (bag->floor_color > -1)
	{
		ft_printf("Error :\nDouble 'F' key detected");
		gest_error(bag, 0);
	}
	i = 0;
	color.argb[3] = 0;
	while (!ft_isdigit(line[i]))
		i++;
	color.argb[2] = ft_atoi(line + i);
	while (ft_isdigit(line[i]))
		i++;
	while (!ft_isdigit(line[i]))
		i++;
	color.argb[1] = ft_atoi(line + i);
	while (ft_isdigit(line[i]))
		i++;
	while (!ft_isdigit(line[i]))
		i++;
	color.argb[0] = ft_atoi(line + i);
	bag->floor_color = color.color;
	return (bag);
}

t_data	*ceiling_fill(t_data *bag, char *line)
{
	t_color	color;
	int		i;

	if (bag->ceiling_color > -1)
	{
		ft_printf("Error :\nDouble 'C' key detected");
		gest_error(bag, 0);
	}
	i = 0;
	color.argb[3] = 0;
	while (!ft_isdigit(line[i]))
		i++;
	color.argb[2] = ft_atoi(line + i);
	while (ft_isdigit(line[i]))
		i++;
	while (!ft_isdigit(line[i]))
		i++;
	color.argb[1] = ft_atoi(line + i);
	while (ft_isdigit(line[i]))
		i++;
	while (!ft_isdigit(line[i]))
		i++;
	color.argb[0] = ft_atoi(line + i);
	bag->ceiling_color = color.color;
	return (bag);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/31 14:53:40 by ksam              #+#    #+#             */
/*   Updated: 2020/10/29 01:47:50 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

int		get_len_max(t_data *img)
{
	int	i;

	i = 0;
	if (!(img->copy = malloc(sizeof(char *) * (img->countline + 1))))
		return (0);
	img->copy[img->countline] = NULL;
	while (img->worldmap[i])
	{
		if ((int)ft_strlen(img->worldmap[i]) > img->len_max)
			img->len_max = (int)ft_strlen(img->worldmap[i]);
		i++;
	}
	return (1);
}

void	fill_tab(t_data *img, int i)
{
	int	j;

	j = 0;
	while (j < img->len_max)
	{
		img->copy[i][j] = '5';
		j++;
	}
	img->copy[i][j] = '\0';
	j = 0;
	while (img->worldmap[i][j] != '\0')
	{
		if ((img->worldmap[i][j] == '\t') || (img->worldmap[i][j] == ' '))
			img->copy[i][j] = '5';
		else
			img->copy[i][j] = img->worldmap[i][j];
		j++;
	}
}

void	clone_map(t_data *img)
{
	int i;

	i = 0;
	get_len_max(img);
	while (i < img->countline)
	{
		if (!(img->copy[i] = malloc(sizeof(char) * (img->len_max + 1))))
			exit(1);
		fill_tab(img, i);
		i++;
	}
	img->copy[i] = NULL;
}

int		is_valid(t_data *img)
{
	int	x;
	int	y;

	y = 0;
	while (y < img->countline)
	{
		x = 0;
		while (x < img->len_max)
		{
			if (!(is_valid_loop(img, x, y)))
				return (0);
			x++;
		}
		y++;
	}
	return (1);
}

void	check_map(t_data *img)
{
	if (img->swidth == -42 || img->sheight == -42)
	{
		ft_printf("Error :\nResolution error");
		exit(1);
	}
	camera_init(img);
	clone_map(img);
	if (!is_valid(img))
	{
		ft_printf("Error :\nMap not conform");
		gest_error(img, 0);
	}
	alloc_sprite_order(img);
	if (img->cardinal_count != 1)
	{
		ft_printf("Error :\n Only 1 | N | S | E | W | accept");
		gest_error(img, 0);
	}
}

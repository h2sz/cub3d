/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_function3.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/27 05:40:56 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:40:59 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	vector_north(t_data *bag)
{
	bag->dirx = 0;
	bag->diry = -1;
	bag->planex = 0.66;
	bag->planey = 0.0;
}

void	vector_south(t_data *bag)
{
	bag->dirx = 0;
	bag->diry = 1;
	bag->planex = -0.66;
	bag->planey = 0.0;
}

void	vector_east(t_data *bag)
{
	bag->dirx = 1;
	bag->diry = 0;
	bag->planex = 0.0;
	bag->planey = 0.66;
}

void	vector_west(t_data *bag)
{
	bag->dirx = -1;
	bag->diry = 0;
	bag->planex = 0.0;
	bag->planey = -0.66;
}

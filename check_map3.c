/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/27 01:23:09 by ksam              #+#    #+#             */
/*   Updated: 2020/10/28 23:20:41 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

int		is_valid_loop(t_data *img, int x, int y)
{
	if (x == 0 || x == img->len_max)
	{
		if (img->copy[y][x] != '1' && img->copy[y][x] != '5')
			return (0);
	}
	if (y == 0 || y == img->countline)
	{
		if (img->copy[y][x] != '1' && img->copy[y][x] != '5')
			return (0);
	}
	if (img->copy[y][x] != '1' && img->copy[y][x] != '5')
	{
		if (!(is_a_zero(img, x, y)))
			return (0);
	}
	return (1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sprite1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/01 09:46:06 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:25:07 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	draw_sprite_next(t_data *img)
{
	img->tex_y = (int)((img->calc * img->sprite_height) / img->sp_h) / 256;
	if (img->sprite_width * img->tex_y + img->tex_x < img->sprite_width *
		img->sprite_height)
		img->color = img->data_sprite[img->sprite_width *
			img->tex_y + img->tex_x];
}

void	alloc_sprite_order(t_data *img)
{
	img->zbuffer = malloc(sizeof(double) * img->swidth);
	img->sp_order = (int *)malloc(sizeof(int) * (img->sprite_counter + 1));
	img->sp_dist = (double *)malloc(sizeof(double) * (img->sprite_counter + 1));
	img->sp_order[img->sprite_counter] = '\0';
	img->sp_dist[img->sprite_counter] = '\0';
}

void	display_sprite(t_data *img)
{
	img->x = 0;
	while (img->x < img->sprite_counter)
	{
		calculate_sprite(img, img->x);
		draw_sprite(img);
		img->x++;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/05 18:13:42 by ksam              #+#    #+#             */
/*   Updated: 2020/10/29 01:23:55 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	parser(t_data *img, char *av)
{
	int		fd;
	int		ret;
	char	*line;

	ret = -1;
	parse_init(img);
	fd = open(av, O_RDONLY);
	while (ret != 0)
	{
		ret = get_next_line(fd, &line);
		if (!line[0] && img->worldmap)
		{
			ft_printf("Error :\n Invalid line");
			exit(1);
		}
		if_forest(img, line);
		free(line);
		line = NULL;
	}
	img->countline++;
	img->worldmap = ft_realloc(img->worldmap, sizeof(char*)
			* (img->countline + 1), sizeof(char*) * img->countline);
	close(fd);
	check_map(img);
}

void	if_forest(t_data *bag, char *line)
{
	int i;

	i = 0;
	if (line[i] == 'R')
		bag = resolution_fill(bag, line);
	else if (line[i] == 'N' && line[i + 1] == 'O')
		bag = no_texture_fill(bag, line);
	else if (line[i] == 'S' && line[i + 1] == 'O')
		bag = so_texture_fill(bag, line);
	else if (line[i] == 'W' && line[i + 1] == 'E')
		bag = we_texture_fill(bag, line);
	else if (line[i] == 'E' && line[i + 1] == 'A')
		bag = ea_texture_fill(bag, line);
	else if (line[i] == 'S' && line[i + 1] != 'O')
		bag = sprite_texture_fill(bag, line);
	else if (line[i] == 'F' || line[i] == 'C')
		bag = (line[i] == 'F') ? floor_fill(bag, line)
			: ceiling_fill(bag, line);
	else if (ft_isdigit(line[i]) || line[i] == ' ' || line[i] == '\t')
		parsing_map(line, bag);
}

int		find_dot(char *line)
{
	int i;

	i = 0;
	while (line[i] != '.')
		i++;
	return (i);
}

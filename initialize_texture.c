/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_texture.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/30 10:31:57 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:41:11 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#include "includes/cub3d.h"

void	init_texture(t_data *img)
{
	if (!(img->north_ptr = mlx_xpm_file_to_image(img->mlx,
		img->path_to_north_texture, &img->north_width, &img->north_height)))
		gest_error(img, 1);
	img->data_north = (int *)mlx_get_data_addr(img->north_ptr,
		&img->bits_per_pixel, &img->size_line, &img->endian);
	if (!(img->south_ptr = mlx_xpm_file_to_image(img->mlx,
		img->path_to_south_texture, &img->south_width, &img->south_height)))
		gest_error(img, 2);
	img->data_south = (int *)mlx_get_data_addr(img->south_ptr,
		&img->bits_per_pixel, &img->size_line, &img->endian);
	init_texture_next(img);
	ft_memdel((void *)&(img->path_to_east_texture));
	ft_memdel((void *)&(img->path_to_north_texture));
	ft_memdel((void *)&(img->path_to_south_texture));
	ft_memdel((void *)&(img->path_to_west_texture));
	ft_memdel((void *)&(img->path_to_sprite_texture));
}

void	init_texture_next(t_data *img)
{
	if (!(img->west_ptr = mlx_xpm_file_to_image(img->mlx,
		img->path_to_west_texture, &img->west_width, &img->west_height)))
		gest_error(img, 3);
	img->data_west = (int *)mlx_get_data_addr(img->west_ptr,
		&img->bits_per_pixel, &img->size_line, &img->endian);
	if (!(img->east_ptr = mlx_xpm_file_to_image(img->mlx,
		img->path_to_east_texture, &img->east_width, &img->east_height)))
		gest_error(img, 4);
	img->data_east = (int *)mlx_get_data_addr(img->east_ptr,
		&img->bits_per_pixel, &img->size_line, &img->endian);
	if (!(img->sprite_ptr = mlx_xpm_file_to_image(img->mlx,
		img->path_to_sprite_texture, &img->sprite_width, &img->sprite_height)))
		gest_error(img, 5);
	img->data_sprite = (int *)mlx_get_data_addr(img->sprite_ptr,
		&img->bits_per_pixel, &img->size_line, &img->endian);
}

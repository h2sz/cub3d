/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cub3d.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksam <ksam@student.42lyon.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/04 14:51:43 by ksam              #+#    #+#             */
/*   Updated: 2020/10/27 05:30:41 by ksam             ###   ########lyon.fr   */
/*                                                                            */
/* ************************************************************************** */

#ifndef CUB3D_H
# define CUB3D_H
# include <math.h>
# include <stdlib.h>
# include "../minilibx/mlx.h"
# include "../ft_printf/libft/libft.h"
# include "../ft_printf/include/libftprintf.h"
# include "../includes/get_next_line.h"
# include "key_linux.h"
# include <fcntl.h>
# include <stdio.h>
# include <unistd.h>
# include <time.h>

typedef struct		s_data
{
	void			*mlx;
	void			*mlx_win;
	void			*img;
	int				*addr;
	int				bits_per_pixel;
	int				line_length;
	int				endian;
	int				size_line;

	char			**copy;
	int				len_max;
	int				x;
	double			posx;
	double			posy;
	double			dirx;
	double			diry;
	double			planex;
	double			planey;
	int				mapx;
	int				mapy;
	double			sidedistx;
	double			sidedisty;
	double			deltadistx;
	double			deltadisty;
	double			camerax;
	double			rayposx;
	double			rayposy;
	double			raydirx;
	double			raydiry;
	double			perpwalldist;
	int				stepx;
	int				stepy;
	int				hit;
	int				side;
	int				lineheight;
	int				drawstart;
	int				drawend;
	double			wallx;
	double			step;
	double			texpos;
	int				tex_x;
	int				tex_y;
	double			*zbuffer;

	double			time;
	double			oldtime;
	double			frametime;
	double			movespeed;
	double			rotspeed;

	int				color;

	int				swidth;
	int				sheight;

	char			*path_to_north_texture;
	char			*path_to_south_texture;
	char			*path_to_west_texture;
	char			*path_to_east_texture;
	char			*path_to_sprite_texture;

	int				*data_north;
	int				*data_south;
	int				*data_west;
	int				*data_east;
	int				*data_sprite;

	void			*north_ptr;
	void			*south_ptr;
	void			*west_ptr;
	void			*east_ptr;
	void			*sprite_ptr;

	int				north_width;
	int				north_height;
	int				south_width;
	int				south_height;
	int				west_width;
	int				west_height;
	int				east_width;
	int				east_height;
	int				sprite_width;
	int				sprite_height;

	int				sprite_counter;
	int				*sp_order;
	double			*sp_dist;
	double			sp_x;
	double			sp_y;
	double			inv_det;
	double			transx;
	double			transy;
	int				sp_screen;
	int				sp_h;
	int				sp_w;
	int				sp_width;
	int				sp_height;
	int				drawstartx;
	int				drawendx;
	int				drawstarty;
	int				drawendy;
	size_t			calc;

	int				floor_color;
	int				ceiling_color;

	char			**worldmap;
	int				cardinal_count;

	int				countline;
	char			*cardinal;

	int				screenshot;

	int				bool_key;
	int				move_up;
	int				move_down;
	int				rotation_left;
	int				rotation_right;
	int				move_directional_left;
	int				move_directional_right;
}					t_data;

typedef struct		s_sprites
{
	double			x;
	double			y;
	int				num;
}					t_sprites;

typedef union
{
	int				color;
	char			argb[4];
}					t_color;

typedef struct		s_bpm
{
	unsigned char	bitmap_type[2];
	int				file_size;
	short			reserved1;
	short			reserved2;
	unsigned int	offset_bits;
}					t_bpm;

typedef struct		s_bpm2
{
	unsigned int	size_header;
	unsigned int	width;
	unsigned int	height;
	short int		planes;
	short int		bit_count;
	unsigned int	compression;
	unsigned int	image_size;
	unsigned int	ppm_x;
	unsigned int	ppm_y;
	unsigned int	clr_used;
	unsigned int	clr_important;
}					t_bpm2;

int					main(int ac, char **av);
int					hook(t_data img);
void				t_img_dark(t_data *img);
int					close_window(t_data *data);
void				my_mlx_pixel_put(t_data *data, int x, int y, int color);
int					calculate(t_data *img);
void				fps_count(t_data *img);
void				ft_memdel(void **str);
void				parser(t_data *img, char *av);
int					parsing_map(char *line, t_data *bag);
int					count_valid_char(char *line, t_data *img);
t_data				*floor_fill(t_data *bag, char *line);
t_data				*ceiling_fill(t_data *bag, char *line);
t_data				*no_texture_fill(t_data *bag, char *line);
t_data				*so_texture_fill(t_data *bag, char *line);
t_data				*we_texture_fill(t_data *bag, char *line);
t_data				*ea_texture_fill(t_data *bag, char *line);
t_data				*sprite_texture_fill(t_data *bag, char *line);
int					find_dot(char *line);
t_data				*resolution_fill(t_data *bag, char *line);
void				if_forest(t_data *bag, char *line);
int					check_arg(int argc, char **argv, t_data *img);
int					check_arg_next(int argc, char **argv, t_data *img,
															char *ptr);
int					find_cub_extension(char *target);
void				gest_error(t_data *img, int code_error);
void				gest_error_next(t_data *img, int code_error);
void				parse_init(t_data *img);
void				camera_init(t_data *bag);
int					find_cardinal(t_data *bag);
void				vector_north(t_data *bag);
void				vector_south(t_data *bag);
void				vector_east(t_data *bag);
void				vector_west(t_data *bag);
void				display_init(t_data *img);
void				ft_init(t_data *img);
void				window_init(t_data *img);
void				init_texture(t_data *img);
void				init_texture_next(t_data *img);
void				display(t_data *img);
void				display_sprite(t_data *img);
void				setup_raytracer(t_data *img);
void				ray_pos_dir(t_data *img);
void				case_detect(t_data *img);
void				ray_len(t_data *img);
void				step_side_distance(t_data *img);
void				calcul_stepx(t_data *img);
void				calcul_stepy(t_data *img);
void				dda_calculs(t_data *img);
void				dda_algorithm(t_data *img);
void				fisheye_adjustment(t_data *img);
void				height_wall(t_data *img);
void				fill_stripe(t_data *img);
void				print_wall(t_data *img);
void				draw_floor_ceiling(t_data *img);
void				color_wall(t_data *img);
void				verline(t_data *img);
int					event(int keycode, t_data *img);
int					event_key_down(int keycode, t_data *img);
int					event_key_up(int keycode, t_data *img);
void				mouvement_bool(int keycode, t_data *img, int onoff);
void				move(t_data *img);
void				move_down(t_data *img);
void				move_up(t_data *img);
void				rotation_right(t_data *img);
void				rotation_left(t_data *img);
void				move_directional_right(t_data *img);
void				move_directional_left(t_data *img);
void				check_map(t_data *img);
int					check_left(t_data *img, int x, int y);
int					check_right(t_data *img, int x, int y);
int					check_up(t_data *img, int x, int y);
int					check_down(t_data *img, int x, int y);
int					is_a_zero(t_data *img, int x, int y);
int					is_valid_loop(t_data *img, int x, int y);
void				texture(t_data *img);
void				texture_north(t_data *img);
void				texture_south(t_data *img);
void				texture_west(t_data *img);
void				texture_east(t_data *img);
void				bitmap_image(t_data *img, int fd, t_bpm2 bitinf);
void				save_bitmap(char *filename, t_data *img);
void				calculate_sprite(t_data *img, int x);
void				calculate_sprite_next(t_data *img);
void				calculate_sprite_dist(t_data *img, t_sprites *sprites);
void				calculate_sort_dist(t_data *img);
void				draw_sprite(t_data *img);
void				draw_sprite_next(t_data *img);
void				alloc_sprite_order(t_data *img);
int					sprite_casting(t_data *img);

#endif
